package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

import com.epam.rd.java.basic.task7.db.DBManager;

public class Team {

	private int id;

	private String name;

	public int getId() {
		if (id == 0) {
		    id = DBManager.getInstance().GetTeamId(name);	
		}
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team t = new Team();
		t.setId(0);
		t.setName(name);
		return t;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		return Objects.equals(name, other.name);
		//return id == other.id && Objects.equals(name, other.name);
	}

	
}

package com.epam.rd.java.basic.task7;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.epam.rd.java.basic.task7.db.DBException;
import com.epam.rd.java.basic.task7.db.DBManager;
import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;
import com.mysql.cj.protocol.x.SyncFlushDeflaterOutputStream;

public class Main {
    public static void main(String[] args) throws DBException {
    	/*try {
    	DBManager dbm = DBManager.getInstance();
    	List<User> users = createAndInsertUsers(0, 5);
		List<Team> teams = createAndInsertTeams(0, 5);
		for (int j = 0; j < 5; j++) {
			dbm.setTeamsForUser(users.get(j), teams.subList(0, j + 1).toArray(Team[]::new));
		}

		for (int j = 0; j < 5; j++) {
			List<Team> userTeams = sort(dbm.getUserTeams(users.get(j)), Team::getName);
			System.out.println(teams.subList(0, j + 1));
			System.out.println(userTeams);
			System.out.println("~~~");
		}

		System.out.println("==================");
		
		List<Team> userTeams = null; 
		
			dbm.deleteTeam(teams.get(0));
			dbm.deleteTeam(teams.get(1));
			dbm.deleteTeam(teams.get(3));
			userTeams = sort(dbm.getUserTeams(users.get(4)), Team::getName);
		
			System.out.println(asList(teams.get(2), teams.get(4))); 
			System.out.println(userTeams);
    	} catch (DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
    	
    	
    	DBManager dbm = DBManager.getInstance();
    	User user = User.createUser("user");
		Team teamA = Team.createTeam("A");
		Team teamB = Team.createTeam("B");
		Team teamC = Team.createTeam("C");
		Team teamD = Team.createTeam("D");

		dbm.insertUser(user);
		dbm.insertTeam(teamA);
		dbm.insertTeam(teamB);
		dbm.insertTeam(teamC);
		dbm.insertTeam(teamD);

		dbm.setTeamsForUser(user, teamA);
		System.out.println(asList(teamA));
		System.out.println(dbm.getUserTeams(user));

		try {
			// transaction must fail
			System.out.println("try");
			dbm.setTeamsForUser(user, teamB, teamC, teamD, teamA);
			//fail("If a transaction has been failed an exception must be thrown");
		} catch (Exception ex) {
			System.out.println("catch");
			//assertTrue(ex instanceof DBException, "Thrown exception musb be a DBException");
		}
		System.out.println(asList(teamA));
		System.out.println(dbm.getUserTeams(user));


		dbm.setTeamsForUser(user, teamB);
		System.out.println(asList(teamA, teamB));
		System.out.println(sort(dbm.getUserTeams(user), Team::getName));

		try {
			// transaction must fail
			System.out.println("try");
			dbm.setTeamsForUser(user, teamC, teamD, teamB);
			//fail("If a transaction has been failed an exception must be thrown");
		} catch (Exception ex) {
			System.out.println("catch");
			//assertTrue(ex instanceof DBException, "Thrown exception musb be a DBException");
		}
		System.out.println(asList(teamA, teamB));
		System.out.println(sort(dbm.getUserTeams(user), Team::getName));

		dbm.setTeamsForUser(user, teamC, teamD);
		System.out.println(asList(teamA, teamB, teamC, teamD));
		System.out.println(sort(dbm.getUserTeams(user), Team::getName));
    }
    
    private static List<User> createAndInsertUsers(int from, int to) throws DBException {
		DBManager dbm = DBManager.getInstance();
		List<User> users = IntStream.range(from, to)
			.mapToObj(x -> "user" + x)
			.map(User::createUser)
			.collect(Collectors.toList());

		for (User user : users) {
			dbm.insertUser(user);
		}
		return users;
	}
    
    private static List<Team> createAndInsertTeams(int from, int to) throws DBException {
		DBManager dbm = DBManager.getInstance();
		List<Team> teams = IntStream.range(from, to)
			.mapToObj(x -> "team" + x)
			.map(Team::createTeam)
			.collect(Collectors.toList());

		for (Team team : teams) {
			dbm.insertTeam(team);
		}
		return teams;
	}
    
    private static <T, U extends Comparable<? super U>> List<T> 
	    sort(List<T> items, Function<T, U> extractor) {
        items.sort(Comparator.comparing(extractor));
        return items;
    }
    
    private static Object asList(Object... items) {
		return Arrays.asList(items);
	}
}

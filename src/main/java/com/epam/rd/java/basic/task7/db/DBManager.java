package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String INSERT_USER_TEAM = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
	private static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
	private static final String SELECT_ALL_TEAMS = "SELECT id,name FROM teams";
	private static DBManager instance;
    private static final String SELECT_ALL_USERS = "SELECT id,login FROM users";
	private static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    private static final String SELECT_USER_BY_LOGIN = "SELECT id, login from users WHERE login=?";
	private static final String SELECT_TEAM_BY_NAME = "SELECT id, name from teams WHERE name=?";
    private static final String SELECT_USER_TEAMS = "SELECT t1.team_id, t2.name FROM users_teams t1 INNER JOIN teams t2 ON t1.team_id=t2.id WHERE t1.user_id=?";
	private static final String DELETE_USER_TEAMS = "DELETE FROM users_teams WHERE user_id=?";
    private static final String COUNT_USER_TEAMS = "SELECT COUNT(*) FROM users_teams WHERE user_id=?";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE id=?";
    private static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
    private static final String DELETE_USER = "DELETE FROM users WHERE id=?";
    private static final String SELECT_USER_ID = "SELECT id from users WHERE login=?";
    private static final String SELECT_TEAM_ID = "SELECT id from teams WHERE name=?";
    private static final String SELECT_USER_TEAM = "SELECT user_id FROM users_teams WHERE team_id=?";
    
	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new LinkedList<>();
		try (ResultSet rs = getFindAll(SELECT_ALL_USERS);) {	
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt(1));
				u.setLogin(rs.getString(2));
				users.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DBException("findAllUsers() error!", e.getCause());
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		if (getUser(user.getLogin()) != null) {
			return false;
		}
		
		try (PreparedStatement stmt = getOneStatement(INSERT_USER);) {
			stmt.setString(1, user.getLogin());
			return stmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean result = false;
		int res;
		int resCount = 0;
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);
			for (User user : users) {
				try (PreparedStatement stmt = con.prepareStatement(DELETE_USER);) {
					stmt.setInt(1, user.getId());
					res = stmt.executeUpdate();
					if (res == 1) {
						resCount++;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
			if (resCount == users.length) {
				con.commit();
				result = true;
			} else {
				con.rollback();
				throw new SQLException();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public User getUser(String login) throws DBException {
		try (PreparedStatement stmt = getOneStatement(SELECT_USER_BY_LOGIN);) {
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				User u = new User();
				u.setId(rs.getInt(1));
				u.setLogin(rs.getString(2));
				return u;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try (PreparedStatement stmt = getOneStatement(SELECT_TEAM_BY_NAME);) {
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				Team t = new Team();
				t.setId(rs.getInt(1));
				t.setName(rs.getString(2));
				return t;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new LinkedList<>();
		try (ResultSet rs = getFindAll(SELECT_ALL_TEAMS);) {
			while (rs.next()) {
				Team u = new Team();
				u.setId(rs.getInt(1));
				u.setName(rs.getString(2));
				teams.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DBException("findAllUsers() error!", e.getCause());
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (getTeam(team.getName()) != null) {
			return false;
		}
		
		try (PreparedStatement stmt = getOneStatement(INSERT_TEAM);) {
			stmt.setString(1, team.getName());
			return stmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		boolean result = false;
		int res;
		int resCount = 0;
		Connection con = getConnection();
		try {
			if (isTeamsForUser(user, teams)) {
				Exception exc = new SQLException("Transaction failed!");
				throw new DBException("Transaction failed!", exc);
				/*if (!deleteTeamsForUser(user)) {
					con.rollback();
					return result;
				}*/
			}
			con.setAutoCommit(false);
			for (Team team : teams) {
				try (PreparedStatement stmt = con.prepareStatement(INSERT_USER_TEAM);) {
					stmt.setInt(1, user.getId());
					stmt.setInt(2, team.getId());
					res = stmt.executeUpdate();
					if (res == 1) {
						resCount++;
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
			if (resCount == teams.length) {
				con.commit();
				result = true;
			} else {
				con.rollback();
				throw new SQLException();
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			/*try {
				con.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
		
		return result;	
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new LinkedList<>();
		try (PreparedStatement stmt = getOneStatement(SELECT_USER_TEAMS);) {
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Team u = new Team();
				u.setId(rs.getInt(1));
				u.setName(rs.getString(2));
				teams.add(u);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DBException("getUserTeams() error!", e.getCause());
		}
		return teams;	
	}

	public boolean deleteTeam(Team team) throws DBException {
		int res = 0;
		try (PreparedStatement stmt = getOneStatement(DELETE_TEAM);) {
			stmt.setInt(1, team.getId());
			res = stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res == 1;
	}

	public boolean updateTeam(Team team) throws DBException {
		int res = 0;
		try (PreparedStatement stmt = getOneStatement(UPDATE_TEAM);) {
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			res = stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res == 1;
	}

	/**
	 * Get connection URL.
	 * @return url.
	 */
	private String getUrl() {
	    Properties prop = new Properties();
	    try {
			prop.load(new FileInputStream("app.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String res = prop.getProperty("connection.url");
	    //Logger logger = Logger.getLogger(this.getClass().getName());
	    //logger.info(prop.getProperty("connection.url"));
	    return res;
	}
	
	/**
	 * Get result set for findAll queries.
	 * @return
	 * @throws DBException 
	 */
	private ResultSet getFindAll(String query) throws DBException {
		ResultSet rs = null;
		try {
			Connection con = getConnection();
			Statement stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			//con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DBException("getFindAll() error!", e.getCause());
		}
		return rs;
	}
	
	/**
	 * Get connection.
	 * @return
	 */
	private Connection getConnection() {
		Connection con = null;
		try {
			con = DriverManager.getConnection(getUrl());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	/**
	 * Get a statement for one entity (User, Team) queries.
	 * @return
	 * @throws DBException 
	 */
	private PreparedStatement getOneStatement(String query) throws DBException {
		PreparedStatement res = null;
		try {
			Connection con = getConnection();
			res = con.prepareStatement(query);
			//con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new DBException("getFindAll() error!", e.getCause());
		}
		return res;
	}
	
	/**
	 * Check out if there is a user in the table users_teams 
	 * @param user
	 * @return
	 */
	private boolean isTeamsForUser(User user,  Team... teams) {
		boolean res = false;
		Connection con = getConnection();
		for (Team team : teams) {
			try (PreparedStatement stmt = con.prepareStatement(SELECT_USER_TEAM);) {
				stmt.setInt(1, team.getId());
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					if (rs.getInt(1) == user.getId()) {
					    res = true;
					    break;
					}
				}
				if (res) {
					break;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		return res;
		//return countTeamsForUser(user) > 0;
	}
	
	/**
	 * Delete user records from the table users_teams.
	 * @param user
	 * @return
	 */
	private boolean deleteTeamsForUser(User user) {
		boolean result = false;
		int res = 0;
		int count = countTeamsForUser(user);
		if (count > 0) {
			try (PreparedStatement stmt = getOneStatement(DELETE_USER_TEAMS);) {
				stmt.setInt(1, user.getId());
				res = stmt.executeUpdate();
				if (res == count) {
					result = true;	
				}
			} catch (SQLException | DBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result; 
	}
	
	/**
	 * Count number of teams for a user.
	 * @param user
	 * @return
	 */
	private int countTeamsForUser(User user) {
		int res = 0;
		try (PreparedStatement stmt = getOneStatement(COUNT_USER_TEAMS);) {
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				res = rs.getInt(1);
			}
		} catch (SQLException | DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	
	/**
	 * Get user id by login.
	 * @param login
	 * @return
	 */
	public int GetUserId(String login) {
        int id = -1;
        try (PreparedStatement stmt = getOneStatement(SELECT_USER_ID);) {
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException | DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return id;
	}
	
	/**
	 * Get team id by name.
	 * @param name
	 * @return
	 */
	public int GetTeamId(String name) {
        int id = -1;
        try (PreparedStatement stmt = getOneStatement(SELECT_TEAM_ID);) {
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException | DBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return id;
	}
}
